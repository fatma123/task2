<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'dashboard', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware'=>'admin'], function () {
    Route::resource('/employees', 'Admin\EmployeesController');
    Route::get('employees/{id}/activate', 'Admin\EmployeesController@activate')->name('employee.activate');
    Route::get('employees/{id}/deActivate', 'Admin\EmployeesController@deActivate')->name('employee.deActivate');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
