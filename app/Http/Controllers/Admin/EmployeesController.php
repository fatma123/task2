<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees=Employee::paginate(10);
        return view('admin.employees.index',compact('employees'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users=User::pluck('name','id');
        return view('admin.employees.add',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'first_name'=>'required|string|max:20',
            'last_name'=>'required|string|max:20',
            'image' =>'required|mimes:jpg,png,gif,jpeg',
            'user_id'=>'required|exists:users,id',
            'status'=>'required',
            'lat'=>'required',
            'long'=>'required'
        ]);
        $image = uploader($request,'image');
        $inputs=$request->all();
        $inputs['image']=$image;

        Employee::create($inputs);

        alert()->success('تم اضافة بيانات الموظف بنجاح !')->autoclose(5000);

        return redirect('dashboard/employees');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users=User::all();
        $employee=Employee::find($id);
        return view('admin.employees.edit',compact('employee','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee=Employee::find($id);
        $this->validate($request,[
            'first_name'=>'required|string|max:20',
            'last_name'=>'required|string|max:20',
            'job'=>'required|string|min:20',
            'image' =>'nullable|mimes:jpg,png,gif,jpeg',
            'user_id'=>'required|exists:users,id',
            'status'=>'required',
            'lat'=>'required',
            'long'=>'required'
        ]);
        $inputs=$request->all();
        if($request->hasFile('image'))
        {
            $image = uploader($request,'image');
            $inputs['image']=$image;
            deleteImg($employee->image);
        }
        $employee->update($inputs);
        alert()->success('تم تعديل بيانات الموظف بنجاح !')->autoclose(5000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee=Employee::find($id);

        if ($employee){
            deleteImg($employee->image);
            $employee->delete();
            alert()->success('تم حذف لموظف بنجاح');
            return back();
        }
        alert()->error('الموظف التي تحاول حذفه غير موجودة');
        return back();
    }

    public function activate($id)
    {
        $employee =Employee::findOrFail($id);
        $employee->status = 'active';
        $employee->save();
        alert()->success('تم تفعيل حساب الموظف  !')->autoclose(5000);
        return back();
    }


    public function deActivate($id)
    {
        $employee =Employee::findOrFail($id);
        $employee->status = 'not_active';
        $employee->save();
        alert()->success('تم إلغاء تفعيل حساب الموظف  !')->autoclose(5000);
        return back();
    }
}
