@extends('admin.layout')
@section('header')
    @endsection
@section('title')
الصفحة الرئيسية والاحصائيات
@endsection

@section('content')

    <div class="row">
        <div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">
            <div class="panel panel-body bg-warning has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد المستخدمين</span>
                        <h3 class="no-margin">{{count(App\User::where('role','client')->get())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class=" icon-users2 icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">
            <div class="panel panel-body bg-success has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد المديرين</span>
                        <h3 class="no-margin">{{count(App\User::where('role','admin')->get())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-user-tie icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">
            <div class="panel panel-body bg-info has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد الرسائل </span>
                        <h3 class="no-margin">{{App\Contact::all()->count()}}</h3>
                    </div>
                    <div class="media-right media-middle">
                        <i class="icon-envelop3 icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">
            <div class="panel panel-body bg-success has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد الاقسام الرئيسية</span>
                        <h3 class="no-margin">{{count(\App\Category::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-package icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">
            <div class="panel panel-body bg-info has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد الاقسام الفرعيه</span>
                        <h3 class="no-margin">{{count(\App\SubCategory::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-package icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">
            <div class="panel panel-body bg-danger has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini"> عدد المنتجات</span>
                        <h3 class="no-margin">{{count(\App\Product::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-bubble icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">
            <div class="panel panel-body bg-warning has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini"> المنتجات المفضله</span>
                        <h3 class="no-margin">{{count(\App\Favourite::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-bookmark icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">--}}
            {{--<div class="panel panel-body bg-success has-bg-image">--}}
                {{--<div class="media no-margin">--}}
                    {{--<div class="media-body">--}}
                        {{--<span class="text-uppercase text-size-mini">عدد  الكوبونات</span>--}}
                        {{--<h3 class="no-margin">{{count(\App\Coupons::where('store_id',null)->get())}}</h3>--}}

                    {{--</div>--}}

                    {{--<div class="media-right media-middle">--}}
                        {{--<i class="icon-add-to-list icon-3x"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="col-sm-6 col-md-3 col-xs-6 one-statistic wow">
            <div class="panel panel-body bg-info has-bg-image">
                <div class="media no-margin">
                    <div class="media-body">
                        <span class="text-uppercase text-size-mini">عدد طلبات الشراء </span>
                        <h3 class="no-margin">{{count(\App\Cart::all())}}</h3>

                    </div>

                    <div class="media-right media-middle">
                        <i class="icon-add-to-list icon-3x"></i>
                    </div>
                </div>
            </div>
        </div>

    </div>



@endsection


@section('script')
    {!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCsT140mx0UuES7ZwcfY28HuTUrTnDhxww&libraries=places&language=ar') !!}
    <script >
        var startUp = $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        google.maps.event.addDomListener(window, 'load', initMap);
    </script>
@endsection

    @section('script')
        <!-- Theme JS files -->
            <script type="text/javascript" src="/admin/assets/js/plugins/notifications/jgrowl.min.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/ui/moment/moment.min.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/daterangepicker.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/anytime.min.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/pickadate/picker.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
            <script type="text/javascript" src="/admin/assets/js/plugins/pickers/pickadate/legacy.js"></script>
            <script type="text/javascript" src="{{asset('admin/assets/jquery-locationpicker-plugin-master/dist/locationpicker.jquery.js')}}"></script>

            <script type="text/javascript" src="/admin/assets/js/core/app.js"></script>
            <script type="text/javascript" src="/admin/assets/js/pages/picker_date.js"></script>
            <script type="text/javascript" src="{{asset('admin/assets/js/pages/form_layouts.js')}}"></script>
@endsection