<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    لوحة تحكم مخابز الارياف
    |
    @yield('title')</title>
  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
  <link href="{{asset('admin/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/css/core.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/css/components.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/css/extras/animate.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('admin/assets/css/customized.css')}}" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->
  @yield('header')
  <!-- Core JS files -->
  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
  <script type="text/javascript"
          src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBQ9fjKJMbJHN-Xs8zEOIIk6CApqefwMgQ'></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/loaders/pace.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/core/libraries/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/core/libraries/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/loaders/blockui.min.js')}}"></script>
  <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
 {{-- <link href="{{asset('admin/assets/icon-picker/css/fontawesome-iconpicker.min.css')}}" rel="stylesheet">--}}
  <!-- /core JS files -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Theme JS files -->
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/pickers/daterangepicker.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/pagination/bootpag.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/pagination/bs_pagination.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/pagination/datepaginator.min.js')}}"></script>
 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  {{--  @include('sweet::alert')--}}
  <script type="text/javascript" src="{{asset('admin/assets/js/plugins/ui/nicescroll.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/core/app.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/assets/js/pages/components_pagination.js')}}"></script>
  {{--<script type="text/javascript" src="{{asset('admin/assets/js/pages/layout_fixed_custom.js')}}"></script>--}}
  @yield('script')

</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-inverse">
  <div class="navbar-header">
    {{--<a class="navbar-brand" href="/dashboard"><img src="/admin/assets/images/logo.png" alt=""></a>--}}
    <a class="navbar-brand" href="{{url('/dashboard')}}">لوحة تحكم مخابز الارياف</a>

    <ul class="nav navbar-nav visible-xs-block">
      <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
      <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
    </ul>
  </div>

  <div class="navbar-collapse collapse" id="navbar-mobile">
    <ul class="nav navbar-nav">
      <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

    </ul>

    {{--<p class="navbar-text"><span class="label bg-success">متصل اﻵن</span></p>--}}

    <ul class="nav navbar-nav navbar-right">

      {{--<li class="dropdown language-switch">--}}
        {{--<a class="dropdown-toggle" data-toggle="dropdown">--}}
          {{--<img src="{{asset('admin/assets/images/flags/sa.png')}}" class="position-left" alt="">--}}
          {{--عربي--}}
        {{--</a>--}}
      {{--</li>--}}

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <i class="fas fa-bell" title="الاشعارات"></i>
          <span class="visible-xs-inline-block position-right">Notifications</span>
          <span class="badge bg-warning-400">{{auth()->user()->unreadNotifications()->count()}}</span>
        </a>

        <div class="dropdown-menu dropdown-content width-350">
          <ul class="media-list dropdown-content-body">
            @forelse(auth()->user()->unreadNotifications as $notification)
              <li>
                <span class="close_notify"> <a href="{{route('admin.deleteNotification',[$notification->id])}}"><i class="fa fa-times" style=" color: red !important"></i> </a></span>
                <a href="{{route('admin.notifications')}}">
                  <span class="bold"> {{$notification['data']['cart_id']}} </span>
                  <span class="time">{{$notification['created_at']}}</span>
                </a>
              </li>
            @empty
              <li>
                <a href="{{route('admin.notifications')}}">
                  <span class="bold"> لا يوجد طلبات جديده حاليا</span>
                  <span class="time">{{now()}}</span>
                </a>
              </li>
            @endforelse
          </ul>
          <div class="dropdown-content-footer">
            <a href="{{route('admin.notifications')}}" data-popup="tooltip" title="" data-original-title="All notifications"><i class="icon-menu display-block"></i></a>
          </div>
        </div>
      </li>

      <li class="dropdown dropdown-user">
        <a class="dropdown-toggle" data-toggle="dropdown">
          <span>{{auth()->user()->name}}</span>
          <i class="caret"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#" onclick="$('#logout').submit()"><i class="icon-switch2"></i> تسجيل خروج</a></li>
          {!! Form::open(['route'=>'logout','id'=>'logout','class'=>'hide']) !!}
          {!! Form::close() !!}
        </ul>
      </li>



    </ul>
  </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">
    @include('admin.nav')
    <!-- Main content -->
      <div class="content-wrapper">
        @include('admin.breadcrumb')
        <!-- Content area -->
          <div class="content">
            @if(Session::has('success'))
              <div class="alert  alert-styled-left" style="background-color:#ce8936;border-color: #dc9846;color: white">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">اغلاق</span></button>
                <span class="text-semibold"></span>عظيم !<a href="#" class="alert-link"> {{ Session::get('success') }} </a> .
              </div>
          @endif

            @if(Session::has('error'))

                <div class="alert bg-warning alert-styled-left">
                  <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">اغلاق</span></button>
                  <span class="text-semibold"></span>عفواً !<a href="#" class="alert-link"> {{ Session::get('error') }} </a> .
                </div>
            @endif

            @yield('content')

            <!-- Footer -->
            <div class="footer text-muted text-center">

              &copy; 2018. <a href="http://panorama-q.com">تم التطوير والبرمجة</a> بواسطة <a href="#" target="_blank">شركة بانوراما القصيم للحلول البرمجية </a>
            </div>
            <!-- /footer -->

          </div>
          <!-- /content area -->

      </div>
      <!-- /main content -->

  </div>
  <!-- /page content -->
</div>

{{--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
@include('sweet::alert')
<!-- /page container -->
{!!  Html::script('admin/assets/bootstrap-fileinput/js/fileinput.min.js')!!}

{!!  Html::script('admin/assets/bootstrap-fileinput/js/fileinput_locale_ar.js')!!}
<script type="text/javascript">
    $(".file_upload_preview").fileinput({
        showUpload: false,
        showRemove: false,
        showCaption: false
    });
    /*** Close Notification ***/
    $(".close_notify").click(function () {
        $(this).closest(" li").fadeOut(500);
    });
</script>
@stack('scripts')
</body>
</html>
