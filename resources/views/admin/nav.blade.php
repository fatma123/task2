<!-- Main sidebar -->
<div class="sidebar sidebar-main ">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left">
                        <img src="" class="img-circle img-sm" alt="">
                    </a>

                    <div class="media-body">
                        <span class="media-heading text-semibold">{{Auth::user()->name}}</span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-pin text-size-small"></i> مدير الموقع
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">


                <ul class="navigation navigation-main navigation-accordion">


                    <!-- Main -->
                    <li class="navigation-header"><span>اعدادات النظام</span> <i class="icon-menu"
                    title="Main pages"></i></li>


                    <li class="{{(Request::is('/') ? 'active' : '')}}"><a href="{{asset('/')}}"><i
                                    class="icon-home4"></i> <span>موقع أرياف</span></a></li>

                    <li class="{{(Request::is('dashboard') ? 'active' : '')}}"><a href="{{asset('dashboard')}}"><i
                    class="icon-home4"></i> <span>الصفحة الرئيسية</span></a></li>


                    <li class="{{(Request::is('dashboard/contacts') ? 'active' : '')}}"><a href="{{asset('dashboard/contacts')}}"><i
                                    class="icon-envelop"></i> <span>الرسائل  </span></a></li>
                    <li>
                        <a href="#"><i class=" icon-package"></i> <span>البنرات الدعائية</span></a>
                        <ul>
                            <li class="{{(Request::is('dashboard/media') ? 'active' : '')}}"><a
                                        href="{{route('admin.media.index')}}"><i class="icon-list"></i> كل البنرات</a>
                            </li>
                            <li class="{{ (Request::is('dashboard/media/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.media.create')}}"><i class="icon-add-to-list"></i> اضافة
                                    بنر جديد </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class=" icon-package"></i> <span>الأخبار</span></a>
                        <ul>
                            <li class="{{(Request::is('dashboard/news') ? 'active' : '')}}"><a
                                        href="{{route('admin.news.index')}}"><i class="icon-list"></i> كل الأخبار</a>
                            </li>
                            <li class="{{ (Request::is('dashboard/news/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.news.create')}}"><i class="icon-add-to-list"></i> اضافة
                                    خبر جديد </a>
                            </li>
                        </ul>
                    </li>



                    <li class="navigation-header"><span>الأقسام  </span> <i class="icon-menu" title="Main pages"></i></li>

                    <li>
                        <a href="#"><i class=" icon-package"></i> <span>الأقسام الرئيسية</span></a>
                        <ul>
                            <li class="{{(Request::is('dashboard/categories') ? 'active' : '')}}"><a
                                        href="{{route('admin.categories.index')}}"><i class="icon-list"></i> كل الاقسام</a>
                            </li>
                            <li class="{{ (Request::is('dashboard/categories/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.categories.create')}}"><i class="icon-add-to-list"></i> اضافة
                                    قسم رئيسى </a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="#"><i class=" icon-package"></i> <span>الأقسام الفرعية</span></a>
                        <ul>
                            <li class="{{(Request::is('dashboard/subcategories') ? 'active' : '')}}"><a
                                        href="{{route('admin.subcategories.index')}}"><i class="icon-list"></i> كل الاقسام</a>
                            </li>
                            <li class="{{ (Request::is('dashboard/subcategories/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.subcategories.create')}}"><i class="icon-add-to-list"></i> اضافة
                                    قسم  فرعى جديد </a>
                            </li>
                        </ul>
                    </li>
                    <li class="navigation-header"><span>المستخدمين  </span> <i class="icon-menu" title="Main pages"></i></li>


                    <li>
                        <a href="#"><i class="icon-users2"></i> <span>أعضاء الإدارة</span></a>
                        <ul>

                            <li class="{{(Request::is('dashboard/admins') ? 'active' : '')}}"><a
                                        href="{{route('admin.admins.index')}}"><i class="icon-list"></i> كل عضاء الإدارة</a>
                            </li>

                            <li class="{{ (Request::is('dashboard/admins/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.admins.create')}}"><i class="icon-user-plus"></i> اضافة عضو إدارة
                                    جديد</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-users2"></i> <span>المدراء المساعدين</span></a>
                        <ul>
                            <li class="{{(Request::is('dashboard/assistant') ? 'active' : '')}}"><a
                                        href="{{route('admin.assistant.index')}}"><i class="icon-list"></i> كل المديرين </a>
                            </li>
                            <li class="{{ (Request::is('dashboard/assistant/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.assistant.create')}}"><i class="icon-user-plus"></i> اضافة مدير
                                    جديد</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="icon-users2"></i> <span>العملاء</span></a>
                        <ul>

                            <li class="{{(Request::is('dashboard/client') ? 'active' : '')}}"><a
                                        href="{{route('admin.client.index')}}"><i class="icon-list"></i> كل العملاء</a>
                            </li>

                            <li class="{{ (Request::is('dashboard/client/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.client.create')}}"><i class="icon-user-plus"></i> اضافة عميل
                                    جديد</a>
                            </li>

                        </ul>
                    </li>

                    <li class="{{(Request::is('dashboard/permissions') || Request::is('dashboard/permissions/*')    ? 'active' : '')}}">
                        <a href="{{route('admin.permissions.index')}}">
                            <i class="fas fa-cart-arrow-down"></i> <span>الصلاحيات</span></a></li>


                    <li class="navigation-header"><span>المخبز  </span> <i class="icon-menu" title="Main pages"></i></li>

                    <li>
                        <a href="#"><i class=" icon-package"></i> <span> المنتجات</span></a>
                        <ul>
                            <li class="{{(Request::is('dashboard/products') ? 'active' : '')}}"><a
                                        href="{{route('admin.products.index')}}"><i class="icon-list"></i> كل المنتجات</a>
                            </li>
                            <li class="{{ (Request::is('dashboard/products/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.products.create')}}"><i class="icon-add-to-list"></i> اضافة
                                     منتج </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-package"></i> <span> كوبونات الخصم</span></a>
                        <ul>
                            <li class="{{ (Request::is('dashboard/coupons') ? 'active' : '')}}"><a
                                        href="{{route('admin.coupons.index')}}"><i class="icon-list"></i> كل الكوبونات</a>
                            </li>
                            <li class="{{  (Request::is('dashboard/coupons/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.coupons.create')}}"><i class="icon-user-plus"></i> اضافة كوبون
                                    جديد</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class=" icon-location4"></i> <span> الفروع</span></a>
                        <ul>
                            <li class="{{(Request::is('dashboard/branches') ? 'active' : '')}}"><a
                                        href="{{route('admin.branches.index')}}"><i class="icon-list"></i> كل الفروع</a>
                            </li>
                            <li class="{{ (Request::is('dashboard/branches/create') ? 'active' : '')}}"><a
                                        href="{{route('admin.branches.create')}}"><i class="icon-add-to-list"></i> اضافة
                                    فرع جديد </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class=" icon-users2"></i> <span> الطلبات</span></a>
                        <ul>
                            <li class="{{(Request::is('dashboard/carts') ? 'active' : '')}}"><a
                                        href="{{route('admin.carts.index')}}"><i class="icon-list"></i> كل الطلبات</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-heart5"></i> <span> المفضلة</span></a>
                        <ul>
                            <li class="{{ (Request::is('dashboard/favourites') ? 'active' : '')}}"><a
                                        href="{{route('admin.favourites.index')}}"><i class="icon-list"></i> كل المنتجات المفضلة</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-stars"></i> <span> التقييمات</span></a>
                        <ul>
                            <li class="{{ (Request::is('dashboard/rates') ? 'active' : '')}}"><a
                                        href="{{route('admin.rates.index')}}"><i class="icon-list"></i>  عرض تقييمات المنتجات  </a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-stars"></i> <span> التحويلات</span></a>
                        <ul>
                            <li class="{{ (Request::is('dashboard/trans') ? 'active' : '')}}"><a
                                        href="{{route('admin.trans.index')}}"><i class="icon-list"></i>  عرض التحويلات  </a>
                            </li>

                        </ul>
                    </li>
                    {{--<li>--}}
                        {{--<a href="#"><i class="icon-stars"></i> <span> المحادثات</span></a>--}}
                        {{--<ul>--}}
                            {{--<li class="{{ (Request::is('dashboard/chat',[auth()->user()->id]) ? 'active' : '')}}"><a--}}
                                        {{--href="{{route('admin.chat.show',[auth()->user()->id])}}"><i class="icon-list"></i>  عرض المحادثات  </a>--}}
                            {{--</li>--}}

                        {{--</ul>--}}
                    {{--</li>--}}

                    <li class="navigation-header"><span>اعدادت النظام </span> <i class="icon-menu"
                                                                                 title="Main pages"></i></li>
                    <li>
                        <a href="#"><i class="fas fa-cogs">الإعدادات العامة</i> <span></span></a>
                        <ul>

                            <li class="{{(Request::is('dashboard/settings') ? 'active' : '')}}"><a
                                        href="{{route('admin.settings.index','general')}}"><i class="icon-list"></i> كل
                                    الإعدادات</a>
                            </li>


                        </ul>
                    </li>

                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->
