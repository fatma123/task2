@extends('admin.layout')
@section('title')
    الموظفين
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">كل الموظفين</h5>
            <div class="heading-elements">
                <ul class="icons-list">

                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            عرض كل الموظفين والتحكم بهم وبكل العمليات الخاصة بهم مع امكانية البحث وتصدير تقارير وملفات وطباعتهم
        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th> الإسم الأول  </th>
                <th> الإسم الثاني </th>
                <th> الوظيفة </th>
                <th> حاله العضوية </th>
                <th>تغيير حالة العضوية </th>
                <th> العمليات </th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->first_name}}</td>
                    <td>{{$item->second_name}}</td>
                    <td>{{$item->job}}</td>
                    <td>
                        @switch($item->status)
                            @case('active')
                            <span class="label bg-success-400">نشط</span>
                            @break

                            @case('not_active')
                            <span class="label bg-success-400">غير مفعل</span>
                            @break

                            @default
                            غير معروف
                        @endswitch

                    </td>
                    <td>

                        @if($item->status== 'active')
                            <a href="{{url('dashboard/employees/'.$item->id.'/deActivate')}}"
                               class="btn btn-xs btn-danger"><i class="fa fa-times"></i>  </a>
                        @else
                            <a href="{{url('dashboard/employees/'.$item->id.'/activate')}}"
                               class="btn btn-xs btn-success"><i class="fa fa-check"></i> </a>
                        @endif
                    </td>



                    {!!Form::open( ['route' => ['admin.employees.destroy',$item->id] ,'id'=>'delete-form'.$admin->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}
                    <td>
                        <a href="{{route('admin.employees.edit',['id'=>$item->id])}}" data-toggle="tooltip" data-original-title="تعديل"> <i class="icon-pencil7 text-inverse" style="margin-left: 10px"></i> </a>
                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف"> <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا الموظف ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف الموظف تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }
    </script>


@endsection
@section('script')
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/pages/datatables_extension_buttons_init.js')}}"></script>
@endsection
