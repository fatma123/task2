
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class="form-group col-md-6 pull-left">
    <label>إسم الأول</label>
    {!! Form::text("first_name",null,['class'=>'form-control','placeholder'=>'أكتب هنا اسم الموظف الأول'])!!}
</div>

<div class="form-group col-md-6 pull-left">
    <label>إسم الثاني</label>
    {!! Form::text("second_name",null,['class'=>'form-control','placeholder'=>'أكتب هنا اسم الموظف الثاني'])!!}
</div>


<div class="form-group col-md-6 pull-left">
    <label>الوظيفة</label>
    {!! Form::text("job",null,['class'=>'form-control','placeholder'=>'أكتب هنا المسمى الوظيفي'])!!}
</div>




<div class="form-group col-md-6 pull-left">
    <label>    صورة الموظف  </label>
    {!! Form::file("image",null,['class'=>'form-control '])!!}
    @if (isset($employee))
        <img src="{{getimg($employee->image)}}" class="img-preview">
    @endif
</div>


<div class="form-group col-md-12 pull-left">

    <label>    الموقع على الخريطة  </label>

    <div class="form-group">
        <div id="map" style="width: 100%; height: 300px;"></div>

        <div class="clearfix">&nbsp;</div>
        <div class="m-t-small">
            <div class="col-sm-4">
                <label class="p-r-small control-label">خط الطول</label>
            </div>
            <div class="col-sm-6">
                {{ Form::text('lat', null,['id'=>'us_restaurant-lat','class'=>'form-control']) }}
            </div>
            <div class="col-sm-4">
                <label class="p-r-small  control-label">خط العرض </label>
            </div>
            <div class="col-sm-6">
                {{ Form::text('long', null,['id'=>'us_restaurant-lon','class'=>'form-control']) }}
            </div>
            <div class="col-sm-4">
                <label class="p-r-small  control-label"> العنوان </label>
            </div>
            <div class="col-sm-6">
                {{ Form::text('address', null,['id'=>'us_restaurant-address','class'=>'form-control']) }}
            </div>
        </div>
    </div>
</div>

<div class="text-center col-md-12">
    <div class="text-right">
        <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
    </div>
</div>
<script src="{{asset('admin\assets\js\plugins\pickers\location\location.js')}}"></script>

@push('scripts')
    <script>


        $('#map').locationpicker({
            location: {
                latitude: "{{isset($user) ? $user->lat : 31.037933}}",
                longitude:"{{isset($user) ? $user->long : 31.381523}}"
            },
            radius: 300,
            inputBinding: {
                latitudeInput: $('#us_restaurant-lat'),
                longitudeInput: $('#us_restaurant-lon'),
                locationNameInput: $('#us_restaurant-address')
            },
            enableAutocomplete: false,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });

    </script>
@endpush